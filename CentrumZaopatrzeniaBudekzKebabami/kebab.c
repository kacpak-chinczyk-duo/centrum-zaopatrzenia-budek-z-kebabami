﻿#include <stdio.h>
#include "kebab.h"

// Funkcja wczytująca dane
KebabU* wczytaj_dane(FILE *we, size_t rozmiar) {
    size_t i;                                               // Zmienna pomocnicza oznaczająca kolejne elementy tablicy
    KebabU* dane;                                           // Przyszła tablica kebabów
    dane = (KebabU*)malloc(rozmiar * sizeof(KebabU));       //  i rezerwacja dla niej miejsca

    for (i = 0; i < rozmiar; i++) {
        dane[i].kształt = fgetc(we);                        // Wczytuje pierwszy char z linii, który oznacza kształt

        if (dane[i].kształt == WALEC) {
            fscanf(we, " %f %f %f ", &dane[i].kebaby.walec.masa, &dane[i].kebaby.walec.wysokość, &dane[i].kebaby.walec.promień);

        } else if (dane[i].kształt == PROSTOPADŁOŚCIAN) {
            fscanf(we, " %f %f %f ", &dane[i].kebaby.prostopadłościan.masa, &dane[i].kebaby.prostopadłościan.wysokość, &dane[i].kebaby.prostopadłościan.szerokość, &dane[i].kebaby.prostopadłościan.głębokość);

        } else if (dane[i].kształt == GRANIASTOSŁUP) {
            fscanf(we, " %f %f %f ", &dane[i].kebaby.graniastosłup.masa, &dane[i].kebaby.graniastosłup.wysokość, &dane[i].kebaby.graniastosłup.długość_krawędzi_podstawy);

        }
    }

    return dane;
}