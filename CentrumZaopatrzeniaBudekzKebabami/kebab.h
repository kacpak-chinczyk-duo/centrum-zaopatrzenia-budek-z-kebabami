﻿#ifndef KEBAB_HEADER
#define KEBAB_HEADER
#include <stdio.h>

// Typ wyliczeniowy kształtu stosu mięsnego
typedef enum { WALEC = 'W', PROSTOPADŁOŚCIAN = 'P', GRANIASTOSŁUP = 'T' } KształtKebaba;

// Struktura dla stosu kształtu walca
typedef struct {
	float masa, wysokość;
    float promień;
} KebabW;

// Struktura dla stosu kształtu prostopadłościanu
typedef struct {
    float masa, wysokość;
    float szerokość, głębokość;
} KebabP;

// Struktura dla stosu kształtu graniastosłupa o podstawie trójkątnej
typedef struct {
    float masa, wysokość;
    float długość_krawędzi_podstawy;
} KebabT;

// Strukta unifikująca typy kebabów w jeden. Zamiera unię typów stosów mięsnych oraz kształt pozwalający na późniejsze określenie do której zmiennej się odwoływać.
typedef struct {
    KształtKebaba kształt;
    union {
	    KebabW walec;
	    KebabP prostopadłościan;
	    KebabT graniastosłup;
    } kebaby;
} KebabU;

// Prototyp funkcji wczytującej dane, do zaimplementowania w pliku .c
KebabU *wczytaj_dane(FILE *we, size_t rozmiar);

#endif