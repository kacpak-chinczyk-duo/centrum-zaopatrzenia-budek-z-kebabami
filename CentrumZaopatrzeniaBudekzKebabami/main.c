﻿/*
Napisać program generujący raport statystyczny dla transportu mięsa (w postaci stosów) z Centrum Zaopatrzenia Budek z Kebabami, 
który powinien zawierać sumaryczną liczbę stosów, średnią wysokość stosu, sumaryczną objętość oraz masę. 
W związku ze wzrostem popytu na kebaby w Polsce, Centrum Zaopatrzenia Budek z Kebabami rozpoczęlo dystrybucję stosów mięsa 
w kształcie walców (W), prostopadłościanów (P) oraz graniastosłupów o podstawie trójkątnej (T).

Specyfikacja zestawienia transportu jest teraz następująca:
    W masa wysokość promień
    P masa wysokość szerokość głębokość
    T masa wysokość długość_krawędzi_podstawy

Stosy powinny być reprezentowane w programie jako obiekty odpowiednich struktur.

Etapy rozwiązania (po 1 pkt.):
    1.   Struktury KebabW, KebabP oraz KebabT dla poszczególnych kształtów stosów.
    2.   Unia KebabU zawierająca trzy ww. typy kebabów oraz obiekt typu wyliczeniowego okreslający kształt przechowywanego kebaba.
    3.   Funkcja KebabU *wczytaj_dane (FILE *we, size_t &rozmiar) z dynamiczną realokacją pamięci (1,5 pkt.).
    4.   Definicja struktury StatParam przechowującej obliczoną statystykę (średnią wysokość stosu wraz z odchyleniem, sumaryczną objętość oraz masę).
    5.   Funkcja StatParam oblicz_statystyke (const KebabU *tk, size_t n) obliczająca i zwracająca wymagane informacje statystyczne (1,5 pkt.).
    6.   Instrukcje funkcji main() wywołujące sensownie powyższe funkcje.
    7.   Wyświetlenie wyników analizy na ekranie.
    8.   Rozdzielić ww. program na trzy pliki: main.c, kebab.h i kebab.c.
*/

#include <stdio.h>
#include "kebab.h"

int main() {
    int rozmiar = 10;
    FILE* plik = fopen("dane.txt", "r");            // Otwieram plik z danymi z uprawnieniami READ
    KebabU* dane = wczytaj_dane(plik, rozmiar);     // Wczytuje dane z wejścia

    while (rozmiar > 0) {
        if (dane[rozmiar].kształt == WALEC)
            printf("W masa = %.3f; wysokosc = %.3f; promien = %.3f\n", dane[rozmiar].kebaby.walec.masa, dane[rozmiar].kebaby.walec.wysokość, dane[rozmiar].kebaby.walec.promień);
        else if (dane[rozmiar].kształt == PROSTOPADŁOŚCIAN)
            printf("P masa = %.3f; wysokosc = %.3f; szerokosc = %.3f; glebokosc = %.3f\n", dane[rozmiar].kebaby.prostopadłościan.masa, dane[rozmiar].kebaby.prostopadłościan.wysokość, dane[rozmiar].kebaby.prostopadłościan.szerokość, dane[rozmiar].kebaby.prostopadłościan.głębokość);
        else if (dane[rozmiar].kształt == GRANIASTOSŁUP)
            printf("T masa = %.3f; wysokosc = %.3f; krawedz = %.3f\n", dane[rozmiar].kebaby.graniastosłup.masa, dane[rozmiar].kebaby.graniastosłup.wysokość, dane[rozmiar].kebaby.graniastosłup.długość_krawędzi_podstawy);

        rozmiar--;
    }
    getch();
}